package com.alehstech.retrofitexample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alehstech.retrofitexample.R;
import com.alehstech.retrofitexample.model.ImageModel;

import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends
        RecyclerView.Adapter<ImageAdapter.ImageViewHolder>{

    Context mContext;
    protected LayoutInflater vi;
    List<ImageModel>  imageList = new ArrayList<>();

    public ImageAdapter(Context context,List<ImageModel> imageList) {
        mContext = context;
        this.imageList = imageList;

    }

    @NonNull
    @Override
    public ImageAdapter.ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.image_item, parent, false);
        return new ImageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ImageAdapter.ImageViewHolder viewHolder,
                                 final int position) {

        ImageModel item = imageList.get(position);
        String authorName = item.getAuthor();

        if(authorName!=null){
            viewHolder.tvAuthName.setText(authorName);
        }


    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {
        TextView tvAuthName;
        ImageView imvPic;


        private ImageViewHolder(View itemView) {
            super(itemView);
            tvAuthName = (TextView) itemView.findViewById(R.id.tv_auth_name);
            imvPic  = itemView.findViewById(R.id.imv_pic);
        }
    }

}
