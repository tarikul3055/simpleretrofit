package com.alehstech.retrofitexample;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alehstech.retrofitexample.adapter.ImageAdapter;
import com.alehstech.retrofitexample.model.ImageModel;
import com.alehstech.retrofitexample.retrofit.ApiService;
import com.alehstech.retrofitexample.retrofit.RetroClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    RecyclerView rcvImage;
    ProgressBar progress;
    LinearLayoutManager linearLayoutManager;
    ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    public void initView(){
        rcvImage = findViewById(R.id.rcv_image);
        progress = findViewById(R.id.progress);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcvImage.setLayoutManager(linearLayoutManager);

        rcvImage.setItemAnimator(new DefaultItemAnimator());
        getImageList();

    }



    public void getImageList(){

        //Creating an object of our api interface
        ApiService api = RetroClient.getApiService();

        /**
         * Calling JSON
         */
        Call<List<ImageModel>> call = api.getImageList();

        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<List<ImageModel>>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<List<ImageModel>> call, Response<List<ImageModel>> response) {
                //Dismiss Dialog


                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    progress.setVisibility(View.GONE);

                    List<ImageModel> imageModelList = response.body();

                    if(imageModelList.size()>0){
                        imageAdapter = new ImageAdapter(MainActivity.this,imageModelList);
                        rcvImage.setAdapter(imageAdapter);
                    }else {
                        Toast.makeText(MainActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                    }

                    Log.d("response","success");

                }
            }

            @Override
            public void onFailure(Call<List<ImageModel>> call, Throwable t) {

                progress.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


}